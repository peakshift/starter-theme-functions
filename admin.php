<?php
//----------------------------------------------------------------------------------------
// Activate Plugins
//----------------------------------------------------------------------------------------
function runActivatePlugin( $plugin ) {
    $current = get_option( 'active_plugins' );
    $plugin = plugin_basename( trim( $plugin ) );

    if ( !in_array( $plugin, $current ) ) {
        $current[] = $plugin;
        sort( $current );
        do_action( 'activate_plugin', trim( $plugin ) );
        update_option( 'active_plugins', $current );
        do_action( 'activate_' . trim( $plugin ) );
        do_action( 'activated_plugin', trim( $plugin) );
    }
    return null;
}
runActivatePlugin( 'advanced-custom-fields/acf.php' );
//----------------------------------------------------------------------------------------
// Rename Posts: Blog
//----------------------------------------------------------------------------------------
function change_post_menu_label() { global $menu; $menu[5][0] = 'Blog'; echo ''; }
function change_post_object_label() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->name = 'News';
}
add_action( 'admin_menu', 'change_post_menu_label' );
